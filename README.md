<div align="center"><img src="https://design.codeberg.org/logo-kit/stacked.svg"  height="300px"  align="center"></div>

# Ticketing
Project to establish a modern Solution for Codeberg e.V. Support-Requests.

Founded as Taskforce, aimed to renovate the Support Workflow (for Member Management and End-User-Support) in a Ticketsystem at Codeberg.


### ToDos / Milestones:
- [ ] Create a Manifest / SRS
- [ ] Chose a SRS compliant Ticket System
- [ ] Setup
	- [ ] Hosting
	- [ ] Security
	- [ ] Test-Run
	- [ ] Import
- [ ] Announce


### Contributions:
Contributions are really Welcome.

Feel free to [open a new issue](https://codeberg.org/Codeberg-e.V./Ticketing/issues/new) and submit your ideas and thoughts.
All issues will be merged, as relevant, to the final Manifest.


