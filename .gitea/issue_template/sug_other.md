---
name: 'Other suggestion'
about: 'Use for all other'
title: '[Suggestion] '
labels:
  - unspecified
---

Tell us more about your suggestion:

